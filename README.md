# EcomApp

This project is intended to be launch with my [API server](https://gitlab.com/Theo_Tanchoux/ecom-api)

## Requirements 

Node LTS (18)

## Run project

```sh
npm install -g @angular/cli
npm install 
ng serve
```
